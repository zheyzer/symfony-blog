Symfony Standard Edition
========================

Symfony blog based on symfony 3. Use security system, event, console and other symfony features. 

What's inside?
--------------

The Requirement

  * nginx;

  * mysql;

  * rabbitmq;

  * redis;

  * php 7.

For deployment you can use docker(included). 
---------------
Follow steps bottom(minimal docker and docker-compose must have on pc)

## Installation

This project use docker that makes deploy easy. Just do steps under the list
This docker system use ports that's do not used by default.

```bash
docker-compose build
docker-compose up -d
```
Waiting while docker build and run containers

Than you have to install dependies and run migrations and fixtures(optional - test data). Comand list is uder. All comands will be run from container(php-fpm)
Go to php container
```bash
docker-compose exec php bash 
```
Composer install dependies form container
```bash
composer install 
```
Example of application parameters used by default docker config are in file:
#### app/config/parameters.docker.yaml.dist

Next step to create database(it was created than container started. Need add schema)
```bash
./bin/console doctrine:migrations:migrate
```
If you do all from list project have to run.
Open page http://loaclhost:81(docker must be run at this momemt)

##Important remark
Dir var/cache and var/logs have to belong user and group www-data(nginx work-group)
Example command near to change owner
```
chown -R www-data:www-data var/cache var/logs web
```

The web directory set to www-data because we need to upload images there


## Test Data
This step is not necessary, but it can add many added simple data to site. (there are users, post and messages
Additional info: be carefool it will remove database data
```bash
./bin/console doctrine:fixtures:load
```

## Create main user by console
```bash
./bin/console app:create-user
```
User created with data:
username: admin
password: admin

## Usage
Admin panel path: http://localhost:81/admin

## Rabbitmq command listen(consumer run)
```bash
./bin/console rabbitmq:consumer message
```
Now if you send data from contact form(frontend) it's got to rabbitmq and then got to database.

All commands from list above work from container
If all list is done all have to work without problem.

## License
[MIT](https://choosealicense.com/licenses/mit/)