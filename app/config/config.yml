imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: en

framework:
    #esi: ~
    translator: { fallbacks: ['%locale%'] }
    secret: '%secret%'
    router:
        resource: '%kernel.project_dir%/app/config/routing.yml'
        strict_requirements: ~
    form: ~
    csrf_protection: ~
    validation: { enable_annotations: true }
    #serializer: { enable_annotations: true }
    default_locale: '%locale%'
    trusted_hosts: ~
    session:
        # https://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id: snc_redis.session.handler
#        save_path: '%kernel.project_dir%/var/sessions/%kernel.environment%'
    fragments: ~
    http_method_override: true
    assets: ~
    php_errors:
        log: true

# Twig Configuration
twig:
    debug: '%kernel.debug%'
    strict_variables: '%kernel.debug%'
    form_themes: ['bootstrap_4_layout.html.twig']

# Doctrine Configuration
doctrine:
    dbal:
        driver: pdo_mysql
        host: '%database_host%'
        port: '%database_port%'
        dbname: '%database_name%'
        user: '%database_user%'
        password: '%database_password%'
        charset: UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: '%kernel.project_dir%/var/data/data.sqlite'
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #path: '%database_path%'

    orm:
        auto_generate_proxy_classes: '%kernel.debug%'
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Swiftmailer Configuration
swiftmailer:
    transport: '%mailer_transport%'
    host: '%mailer_host%'
    username: '%mailer_user%'
    password: '%mailer_password%'
    spool: { type: memory }

doctrine_migrations:
    dir_name: "%kernel.project_dir%/src/AppBundle/Migrations"
    namespace: AppBundle\Migrations

knp_paginator:
    template:
        pagination: '@KnpPaginator/Pagination/twitter_bootstrap_v4_pagination.html.twig'

vich_uploader:
    db_driver: orm
    mappings:
        post_images:
            uri_prefix:         '/uploads'
            upload_destination: '%kernel.project_dir%/web/uploads'
            namer: vich_uploader.namer_uniqid

snc_redis:
    clients:
        default:
            type: predis
            alias: default
            dsn: redis://%redis_address%
        session:
            type: predis
            alias: session
            dsn: redis://%redis_address%/1
            logging: true
    session:
        client: session
        prefix: session
        ttl: %ttl%


old_sound_rabbit_mq:
    connections:
        default:
            host:     %rabbit_mq_host%
            port:     %rabbit_mq_port%
            user:     %rabbit_mq_user%
            password: %rabbit_mq_pswd%
            vhost:    '/'
            lazy:     true # a lazy connection avoids unnecessary connections to the broker on every request
            connection_timeout: 3
            read_write_timeout: 3
            keepalive: false
            heartbeat: 0
    producers:
        message:
            connection:       default # connects to the default connection configured above
            exchange_options: {name: 'message', type: direct}
    consumers:
        message:
            connection:       default # connects to the default connection configured above
            exchange_options: {name: 'message', type: direct}
            queue_options:    {name: 'message'}
            callback:         AppBundle\Consumer\MessageConsumer # the MessageConsumer defined below
