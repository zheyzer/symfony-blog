<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.05.19
 * Time: 9:44
 */

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{

    public function testLoginAction()
    {

        $client = static::createClient();

        $crawler = $client->request('GET', '/login');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertEquals(1, $crawler->filter('form')->count());

        $form = $crawler->selectButton('Login')->form();

        $form['_username'] = 'admin2';
        $form['_password'] = 'admin2';

        $adminCrawler = $client->submit($form);

        $this->assertTrue(
            $client->getResponse()->isRedirect('http://localhost/admin/')
        );
    }
}
