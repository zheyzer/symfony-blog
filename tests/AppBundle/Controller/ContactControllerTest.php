<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.05.19
 * Time: 9:31
 */

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContactControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact-us');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
