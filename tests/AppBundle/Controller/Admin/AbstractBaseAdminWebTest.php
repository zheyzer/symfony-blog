<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.05.19
 * Time: 15:52
 */

namespace Tests\AppBundle\Controller\Admin;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

abstract class AbstractBaseAdminWebTest extends WebTestCase
{
    protected $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    protected function logIn(string $username, array $roles = ['ROLE_ADMIN'])
    {
        $session = $this->client->getContainer()->get('session');

        $firewallName = 'main';

        $firewallContext = 'main';

        $user = $this->client->getContainer()->get('doctrine')->getRepository(User::class)
            ->findOneBy(['username' => $username]);

        $token = new UsernamePasswordToken($user, null, $firewallName, $roles);

        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    abstract public function testCheckPageAccess();
}
