<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.05.19
 * Time: 16:32
 */

namespace Tests\AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\User;

class UserControllerTest extends AbstractBaseAdminWebTest
{
    public function testCheckPageAccess()
    {
        $this->logIn('manager', ['ROLE_MANAGER']);

        $this->client->request('GET', '/admin/users/');

        $this->assertSame(Response::HTTP_FORBIDDEN, $this->client->getResponse()->getStatusCode());

        $this->logIn('admin2');

        $this->client->request('GET', '/admin/users/');

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider getUrlsForManager
     */
    public function testAccessForManager($httpMethod, $url, $status)
    {
        $this->logIn('manager', ['ROLE_MANAGER']);

        $this->client->request($httpMethod, $url);

        $this->assertSame($status, $this->client->getResponse()->getStatusCode());
    }

    public function getUrlsForManager()
    {
        yield ['GET', '/admin/users/view/1', 403];
        yield ['GET', '/admin/users/edit/1', 403];
        yield ['GET', '/admin/users/delete/1', 403];
    }

    public function testAdminAddUser()
    {
        $this->logIn('admin2');
        $crawler = $this->client->request('GET', '/admin/users/add');

        $username = 'user' . rand();
        $email = $username . '@fixture.com';
        $name = 'Name: ' . $username;
        $password = rand();
        $roles = 'ROLE_MANAGER';

        $form = $crawler->selectButton('Save')->form([
            'username' => $username,
            'email' => $email,
            'name' => $name,
            'plainPassword[first]' => $password,
            'plainPassword[second]' => $password,
            'roles' => $roles
        ]);

        $this->client->submit($form);
        $this->assertSame(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
        $user = $this->client->getContainer()->get('doctrine')->getRepository(User::class)->findOneBy([
            'username' => $username,
        ]);
        $this->assertNotNull($user);
        $this->assertSame($email, $user->getEmail());
    }

    public function testAdminEditUser()
    {
        $this->logIn('admin2');
        $crawler = $this->client->request('GET', '/admin/users/edit/2');

        $username = 'user' . rand();
        $name = 'Name: ' . $username;

        $form = $crawler->selectButton('Save')->form([
            'username' => $username,
            'name' => $name
        ]);

        $this->client->submit($form);
        $this->assertSame(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
        $user = $this->client->getContainer()->get('doctrine')->getRepository(User::class)->findOneBy([
            'username' => $username,
        ]);
        $this->assertNotNull($user);
        $this->assertSame($name, $user->getName());
    }
}
