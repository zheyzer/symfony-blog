<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.05.19
 * Time: 16:32
 */

namespace Tests\AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;

class MessageControllerTest extends AbstractBaseAdminWebTest
{
    public function testCheckPageAccess()
    {
        $this->logIn('manager', ['ROLE_MANAGER']);

        $this->client->request('GET', '/admin/messages/');

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());

        $this->logIn('admin2');

        $this->client->request('GET', '/admin/messages/');

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }


    /**
     * @dataProvider getUrlsForManager
     */
    public function testAccessForManager($httpMethod, $url, $status)
    {
        $this->logIn('manager', ['ROLE_MANAGER']);

        $this->client->request($httpMethod, $url);

        $this->assertSame($status, $this->client->getResponse()->getStatusCode());
    }

    public function getUrlsForManager()
    {
        yield ['GET', '/admin/messages/view/1', 200];
        yield ['GET', '/admin/messages/delete/1', 403];
    }


    /**
     * @dataProvider getUrlsForManager
     */
    public function testAccessForAdmin($httpMethod, $url, $status)
    {
        $this->logIn('manager', ['ROLE_MANAGER']);

        $this->client->request($httpMethod, $url);

        $this->assertSame($status, $this->client->getResponse()->getStatusCode());
    }

    public function getUrlsForAdmin()
    {
        yield ['GET', '/admin/messages/view/1', 200];
        yield ['GET', '/admin/messages/delete/1', 302];
    }
}
