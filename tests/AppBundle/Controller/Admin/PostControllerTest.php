<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.05.19
 * Time: 15:52
 */

namespace Tests\AppBundle\Controller\Admin;

use AppBundle\Entity\Post;
use Symfony\Component\HttpFoundation\Response;

class PostControllerTest extends AbstractBaseAdminWebTest
{
    public function testCheckPageAccess()
    {
        $this->logIn('admin2');

        $this->client->request('GET', '/admin/posts/');

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider getUrlsForAdmin
     */
    public function testAccessForAdmin($httpMethod, $url, $status)
    {
        $this->logIn('admin2');

        $this->client->request($httpMethod, $url);

        $this->assertSame($status, $this->client->getResponse()->getStatusCode());
    }

    public function getUrlsForAdmin()
    {
        yield ['GET', '/admin/posts/view/1', 200];
        yield ['GET', '/admin/posts/view/2', 200];
        yield ['GET', '/admin/posts/view/3', 200];
        yield ['GET', '/admin/posts/view/4', 200];
    }

    public function testAdminAddPost()
    {
        $this->logIn('admin2');
        $crawler = $this->client->request('GET', '/admin/posts/add');

        $postTitle = 'Blog Post Title '.mt_rand();
        $postContent = 'Blog Post Content ' .mt_rand();


        $form = $crawler->selectButton('Save')->form([
            'title' => $postTitle,
            'content' => $postContent,
            'status' => 'new',
        ]);

        $this->client->submit($form);
        $this->assertSame(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
        $post = $this->client->getContainer()->get('doctrine')->getRepository(Post::class)->findOneBy([
            'title' => $postTitle,
        ]);
        $this->assertNotNull($post);
        $this->assertSame($postContent, $post->getContent());
    }

    public function testAdminEditPost()
    {
        $this->logIn('admin2');
        $crawler = $this->client->request('GET', '/admin/posts/edit/1');

        $postTitle = 'Blog Change Post Title '.mt_rand();
        $postContent = 'Blog Change Post Content ' .mt_rand();


        $form = $crawler->selectButton('Save')->form([
            'title' => $postTitle,
            'content' => $postContent,
        ]);

        $this->client->submit($form);
        $this->assertSame(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
        $post = $this->client->getContainer()->get('doctrine')->getRepository(Post::class)->find(1);
        $this->assertNotNull($post);
        $this->assertSame($postContent, $post->getContent());
    }
}
