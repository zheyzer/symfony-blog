<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.05.19
 * Time: 9:32
 */

namespace Tests\AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;

class IndexControllerTest extends AbstractBaseAdminWebTest
{

    /**
     * @dataProvider getRedirectUrls
     */
    public function testCheckRedirect($httpMethod, $url)
    {
        $client = static::createClient();
        $client->request($httpMethod, $url);
        $this->assertTrue($client->getResponse()->isRedirect('http://localhost/login'));
    }

    public function getRedirectUrls()
    {
        yield ['GET', '/admin/'];
        yield ['GET', '/admin/posts/'];
        yield ['GET', '/admin/users/'];
        yield ['GET', '/admin/messages/'];
    }

    /**
     * @dataProvider getUrlsForManager
     */
    public function testAccessForManager($httpMethod, $url, $status)
    {
        $this->logIn('manager', ['ROLE_MANAGER']);

        $this->client->request($httpMethod, $url);

        $this->assertSame($status, $this->client->getResponse()->getStatusCode());
    }

    public function getUrlsForManager()
    {
        yield ['GET', '/admin/posts/', 200];
        yield ['GET', '/admin/users/', 403];
        yield ['GET', '/admin/messages/', 200];
    }

    public function testCheckPageAccess()
    {
        $this->logIn('admin2', ['ROLE_ADMIN']);

        $this->client->request('GET', '/admin/');

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }
}
