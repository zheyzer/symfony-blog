<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.05.19
 * Time: 14:29
 */

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{
    public function test404Action()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/post/test');

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider provideUrls
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function provideUrls()
    {
        return [
            ['/post/23'],
            ['/post/24'],
            ['/post/26']
        ];
    }
}
