<?php
declare(strict_types=1);

namespace AppBundle\Command;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends Command
{

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-user';

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this->setDescription('Create a new user.')
            ->setHelp('This command create user admin - main user with all privileges')
            ->addArgument(
                'username',
                InputArgument::REQUIRED,
                'Enter account username'
            )
            ->addArgument(
                'password',
                InputArgument::REQUIRED,
                'Enter account password'
            )
            ->addArgument(
                'email',
                InputArgument::OPTIONAL,
                'Enter account email'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');
        $email = $input->getArgument('email');
        //need first to check on user existing
        if ($this->entityManager->getRepository(User::class)->findBy(['username' => $username])) {
            $output->write('User "' . $username . '" is already exist!' . PHP_EOL);
            return;
        }

        $user = new User();
        $user->setUsername($username);
        $user->setPlainPassword($password);
        $user->setEmail($email ? $email : $username . '@fixture.com');
        $user->setName($username);
        $user->setRoles([User::ROLE_ADMIN]);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $output->write('User "' . $username . '" created.' . PHP_EOL);
    }
}
