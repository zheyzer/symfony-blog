<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Message;
use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    const USERS = [
        [
            'username' => 'admin2',
            'email' => 'admin2@fixture.com',
            'password' => 'admin2',
            'name' => 'Admin',
            'roles' => [User::ROLE_ADMIN]
        ],
        [
            'username' => 'manager',
            'email' => 'manager@fixture.com',
            'password' => 'manager',
            'name' => 'Manager',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'user1',
            'email' => 'user1@fixture.com',
            'password' => 'user1',
            'name' => 'User1',
            'roles' => [User::ROLE_MANAGER]
        ],
        [
            'username' => 'user2',
            'email' => 'user2@fixture.com',
            'password' => 'user2',
            'name' => 'User2',
            'roles' => [User::ROLE_MANAGER]
        ]
    ];


    /**
     * @var \Faker\Factory
     */
    private $faker;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->faker = \Faker\Factory::create();
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadPosts($manager);
        $this->loadMessages($manager);
    }

    public function loadPosts(ObjectManager $manager)
    {
        for ($i=0; $i<30; $i++) {
            $post = new Post();

            $post->setTitle($this->faker->realText('64'));
            $post->setContent($this->faker->realText());

            $createdAt = $this->faker->dateTimeThisYear();

            $post->setCreatedAt($createdAt);
            $post->setOwner($this->getReference('user_' . random_int(0, 3)));
            $post->setStatus('published');

            $manager->persist($post);
        }

        $manager->flush();
    }

    public function loadUsers(ObjectManager $manager)
    {
        foreach (self::USERS as $key => $userFixture) {
            $user = new User();
            $user->setUsername($userFixture['username']);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $userFixture['password']));
            $user->setEmail($userFixture['email']);
            $user->setName($userFixture['name']);
            $user->setRoles($userFixture['roles']);
            $this->addReference('user_' . $key, $user);

            $manager->persist($user);
        }

        $manager->flush();
    }

    public function loadMessages(ObjectManager $manager)
    {
        for ($i=0; $i<30; $i++) {
            $message = new Message();

            $message->setName($this->faker->name());
            $message->setEmail($this->faker->email);
            $message->setContent($this->faker->realText());
            $manager->persist($message);
        }
        $manager->flush();
    }
}
