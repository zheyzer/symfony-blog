<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.05.19
 * Time: 18:20
 */
declare(strict_types=1);

namespace AppBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ExcerptExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('excerpt', [$this, 'formatExcerpt'])
        ];
    }

    public function formatExcerpt(string $text, $length = 60)
    {
        if (strlen($text) < $length) {
            return $text;
        }

        return substr($text, 0, $length) . '...';
    }
}
