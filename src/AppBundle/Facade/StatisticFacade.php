<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.05.19
 * Time: 12:32
 */
declare(strict_types=1);

namespace AppBundle\Facade;

use AppBundle\Repository\MessageRepository;
use AppBundle\Repository\PostRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class StatisticFacade
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var PostRepository
     */
    private $postRepository;
    /**
     * @var MessageRepository
     */
    private $messageRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        PostRepository $postRepository,
        MessageRepository $messageRepository,
        UserRepository $userRepository
    ) {
        $this->entityManager = $entityManager;
        $this->postRepository = $postRepository;
        $this->messageRepository = $messageRepository;
        $this->userRepository = $userRepository;
    }

    public function getCountPosts(): string
    {
        return $this->postRepository->getCountItems();
    }

    public function getCountMessages(): string
    {
        return $this->messageRepository->getCountItems();
    }

    public function getCountUsers(): string
    {
        return $this->userRepository->getCountItems();
    }

    public function getAllStatistic(): array
    {
        return [
            'posts' => $this->getCountPosts(),
            'users' => $this->getCountUsers(),
            'messages' => $this->getCountMessages()
        ];
    }
}
