<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.05.19
 * Time: 10:00
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Message;
use AppBundle\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/messages")
 */
class MessageController extends Controller
{
    /**
     * @Route("/", name="admin_message_index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param MessageRepository $messageRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, PaginatorInterface $paginator, MessageRepository $messageRepository)
    {
        $queryMessages = $messageRepository->getQueryMessages();

        $messages = $paginator->paginate(
            $queryMessages,
            $request->query->getInt('page', 1),
            Message::NUMBER_OF_ITEMS
        );

        return $this->render('admin/message/index.html.twig', [
            'messages' => $messages
        ]);
    }

    /**
     * @Route("/view/{id}", name="admin_message_view")
     * @param Message $message
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Message $message)
    {
        return $this->render('admin/message/view.html.twig', [
            'message' => $message
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin_message_delete")
     * @IsGranted("ROLE_ADMIN", message="Access Denied to delete messages")
     * @param Message $message
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Message $message, EntityManagerInterface $entityManager)
    {
        $messageId = $message->getId();
        $entityManager->remove($message);
        $entityManager->flush();

        $this->addFlash('notice', 'Message #' . $messageId . ' deleted');

        return $this->redirectToRoute('admin_message_index');
    }
}
