<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.05.19
 * Time: 12:18
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostType;
use AppBundle\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostController
 * @package AppBundle\Controller\Admin
 * @Route("/admin/posts")
 */
class PostController extends Controller
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="admin_post_index")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param PostRepository $postRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(PaginatorInterface $paginator, Request $request, PostRepository $postRepository)
    {
        if (in_array(User::ROLE_ADMIN, $this->getUser()->getRoles())) {
            $postsQuery = $postRepository->getQueryPosts($request->query->all());
        } else {
            $postsQuery = $postRepository->getQueryPostsByUser($this->getUser());
        }

        $posts = $paginator->paginate(
            $postsQuery,
            $request->query->getInt('page', 1),
            POST::NUMBER_OF_ITEMS
        );

        return $this->render('admin/post/index.html.twig', [
            'posts' => $posts
        ]);
    }


    /**
     * @Route("/add", name="admin_post_add")
     * @Security("is_granted('ROLE_MANAGER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request)
    {
        $post = new Post();
        $post->setOwner($this->getUser());

        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($post);
            $this->entityManager->flush();

            $this->addFlash('notice', 'New post added');

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->render('admin/post/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/view/{id}", name="admin_post_view")
     * @Security("is_granted('view', post)")
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Post $post)
    {
        $this->denyAccessUnlessGranted('view', $post);

        return $this->render('admin/post/view.html.twig', [
            'post' => $post
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_post_edit")
     * @Security("is_granted('edit', post)")
     * @param Post $post
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Post $post, Request $request)
    {
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($post);
            $this->entityManager->flush();

            $this->addFlash('notice', 'Post #' . $post->getId() . ' modified');

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->render('admin/post/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin_post_delete")
     * @Security("is_granted('delete', post)")
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Post $post)
    {
        $this->entityManager->remove($post);

        $this->addFlash('notice', 'Post #'.$post->getId(). ' deleted');
        $this->entityManager->flush();

        return $this->redirectToRoute('admin_post_index');
    }
}
