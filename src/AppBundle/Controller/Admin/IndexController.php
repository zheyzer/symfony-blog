<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.05.19
 * Time: 16:48
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Facade\StatisticFacade;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController
 * @package AppBundle\Controller\Admin
 * @Route("/admin")
 */
class IndexController extends Controller
{
    /**
     * @Route("/", name="admin_index")
     * @param StatisticFacade $statisticFacade
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dashboard(StatisticFacade $statisticFacade)
    {
        $stats = $statisticFacade->getAllStatistic();

        return $this->render('admin/dashboard/index.html.twig', [
            'stats' => $stats
        ]);
    }
}
