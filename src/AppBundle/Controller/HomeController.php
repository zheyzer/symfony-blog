<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Repository\PostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param PostRepository $postRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, PaginatorInterface $paginator, PostRepository $postRepository)
    {
        $queryPosts = $postRepository->getQueryPublishedPosts();

        $posts = $paginator->paginate(
            $queryPosts,
            $request->query->getInt('page', 1),
            Post::NUMBER_OF_ITEMS
        );

        return $this->render('home/index.html.twig', [
            'posts' => $posts
        ]);
    }
}
