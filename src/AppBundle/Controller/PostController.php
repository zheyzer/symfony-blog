<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.05.19
 * Time: 11:32
 */

namespace AppBundle\Controller;

use AppBundle\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends Controller
{
    /**
     * @Route("/post/{id}", name="page_post")
     * @param $id
     * @param PostRepository $postRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id, PostRepository $postRepository)
    {
        if (!is_numeric($id)) {
            throw new NotFoundHttpException();
        }

        $post = $postRepository->findPublishedPostById($id);

        if (!$post) {
            throw new NotFoundHttpException();
        }

        return $this->render('post/show.html.twig', [
            'post' => $post
        ]);
    }
}
