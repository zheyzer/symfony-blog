<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.05.19
 * Time: 18:09
 */

namespace AppBundle\Doctrine;

use AppBundle\Entity\Post;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;

class UpdatedDateListener implements EventSubscriber
{

    public function getSubscribedEvents()
    {
        return ['prePersist', 'preUpdate'];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof Post) {
            return;
        }

        $entity->setUpdatedAt(new \DateTime('now'));
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof Post) {
            return;
        }

        $entity->setUpdatedAt(new \DateTime('now'));

        $em = $args->getEntityManager();
        $meta = $em->getClassMetadata(get_class($entity));
        $em->getUnitOfWork()->recomputeSingleEntityChangeSet($meta, $entity);
    }
}
