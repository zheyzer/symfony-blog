<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.05.19
 * Time: 13:19
 */
declare(strict_types=1);

namespace AppBundle\Security;

use AppBundle\Entity\User;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{

    const VIEW = 'view';
    const MANAGE = 'manage';
    const DELETE = 'delete';

    /**
     * @var AccessDecisionManagerInterface
     */
    private $decisionManager;


    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::MANAGE, self::DELETE])) {
            return false;
        }

        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::VIEW:
                return $subject === $user;
            case self::DELETE:
                return $this->canDelete($user, $subject, $token);
            case self::MANAGE:
                return $this->canManage($user, $subject, $token);
        }

        return false;
    }

    private function canDelete(User $user, User $subject, TokenInterface $token): bool
    {
        if ($this->canManage($user, $subject, $token)) {
            if ($user !== $subject) {
                return true;
            } else {
                throw new AccessDeniedException('You can not delete yourself');
            }
        }
        return false;
    }

    private function canManage(User $user, User $subject, TokenInterface $token): bool
    {
        if ($this->decisionManager->decide($token, [User::ROLE_ADMIN])) {
            return true;
        }

        return false;
    }
}
