<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.05.19
 * Time: 12:39
 */
declare(strict_types=1);

namespace AppBundle\Consumer;

use AppBundle\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class MessageConsumer implements ConsumerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function execute(AMQPMessage $msg)
    {
        $data = json_decode($msg->body);
        $message = new Message();

        $message->setName($data->name);
        $message->setEmail($data->email);
        $message->setContent($data->content);

        $this->entityManager->persist($message);
        $this->entityManager->flush();

        echo 'Your message is added. Message #' . $message->getId() . PHP_EOL;
    }
}
